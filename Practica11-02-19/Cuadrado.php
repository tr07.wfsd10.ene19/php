<?php
require_once ("Poligono.php");
class Cuadrado extends Poligono {
    private $lado;

    public function __construct($lado){
        $this->lado = $lado;
    }

    private function getArea() {
        return $this->lado*2;
    }

}