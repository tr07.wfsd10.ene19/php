<?php
require_once ("Poligono.php");
class Triangulo extends Poligono {

    private $base;
    private $altura;

    public function __construct($base,$altura){
        $this->base = $base;
        $this->altura = $altura;
    }

    private function getArea(){
        return $this->base * $this->altura;
    }
}