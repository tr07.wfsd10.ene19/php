<?php

class Capitulo extends ElementoMultimedia {
  public $titulo;
  public $duracion;
  public $sinopsis;
  public $rating;
  public $orden;
  
  function __construct($titulo,$duracion,$sinopsis,$orden,$type){
    $this->titulo = $titulo;
    $this->duracion = $duracion;
    $this->sinopsis = $sinopsis;
    $this->orden = $orden;
    parent::__construct($type);
    
  }


  public setValoracion($rating) {
    $this->rating = $rating;
  }
}